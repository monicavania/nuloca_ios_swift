//
//  FifthViewController.swift
//  final_nuloca
//
//  Created by Monica on 15/11/21.
//

import UIKit

class FifthViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionview1: UICollectionView!
    @IBOutlet weak var collectionview2: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.collectionview1.delegate = self
        self.collectionview1.dataSource = self
//        self.collectionview2.delegate = self
//        self.collectionview2.dataSource = self
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath)
            let img = cell.viewWithTag(1) as! UIImageView
            img.image = UIImage.init(named: "netflix")
            return cell
        } else if indexPath.section == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell2", for: indexPath)
            let img = cell.viewWithTag(1) as! UIImageView
            img.image = UIImage.init(named: "mola")
            return cell
        } else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell3", for: indexPath)
            let img = cell.viewWithTag(1) as! UIImageView
            img.image = UIImage.init(named: "disney")
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            return CGSize.init(width: 80, height: 80)
        } else if indexPath.section == 1 {
            return CGSize.init(width: 80, height: 80)
        }else {
            return CGSize.init(width: 80, height: 80)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
