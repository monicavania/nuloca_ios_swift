//
//  SecondViewController.swift
//  final_nuloca
//
//  Created by Monica on 14/11/21.
//

import UIKit

class SecondViewController: UIViewController {
    @IBOutlet weak var register: UIButton!
    
    @IBOutlet weak var login: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        register.addTarget(self, action: #selector(push), for: .touchUpInside)
        login.addTarget(self, action: #selector(push1), for: .touchUpInside)
    }

    @objc func push(){
        let vc = self.storyboard!.instantiateViewController(withIdentifier:"ThirdViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func push1(){
        let vc = self.storyboard!.instantiateViewController(withIdentifier:"FourthViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
