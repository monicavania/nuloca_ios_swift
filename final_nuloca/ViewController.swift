//
//  ViewController.swift
//  final_nuloca
//
//  Created by Monica on 14/11/21.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        button.addTarget(self, action: #selector(push), for: .touchUpInside)
        
        // Do any additional setup after loading the view.
    }
    @objc func push(){
        let vc = self.storyboard!.instantiateViewController(withIdentifier:"SecondViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }


}

