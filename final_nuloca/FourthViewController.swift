//
//  FourthViewController.swift
//  final_nuloca
//
//  Created by Monica on 15/11/21.
//

import UIKit
import Alamofire

class FourthViewController: UIViewController {
    
    @IBOutlet weak var login: UIButton!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var usernameField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let token = ""

        UserDefaults.standard.set(token, forKey: "token")

        login.addTarget(self, action: #selector(push), for: .touchUpInside)
    }
    
    @objc func push(){
        let parameter = ["username": usernameField.text ?? "", "password": passwordField.text ?? "" ]
        
        AF.request("http://localhost:8080/login", method: .post, parameters: parameter, encoding: JSONEncoding.default).responseJSON {
            response in
            if let data = response.data {
                
                print(response)
                // Convert This in JSON
                do {
                    let responseDecoded = try JSONDecoder().decode(Token.self, from: data)
                    UserDefaults.standard.set(responseDecoded.token, forKey: "tokenUser")
                    
                    //cuma buat print token
//                    let tokenuser = UserDefaults.standard.object(forKey: "tokenUser") as! String
//                    print(tokenuser)
                    
                    let fifthVC = self.storyboard!.instantiateViewController(withIdentifier: "FifthViewController")
                    self.navigationController?.pushViewController(fifthVC, animated: true)
                    
                }catch let error as NSError{
                    print(error)
                }
            }
        }
    }
    struct Token : Decodable {
        var token: String?
    }
}
