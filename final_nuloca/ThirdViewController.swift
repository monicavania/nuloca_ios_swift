//
//  ThirdViewController.swift
//  final_nuloca
//
//  Created by Monica on 14/11/21.
//

import UIKit
import Alamofire

class ThirdViewController: UIViewController {
    
    @IBOutlet weak var confirmpassword: UILabel!
    @IBOutlet weak var register: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        confirmpassword.text = "Confirm Password: "
        confirmpassword.numberOfLines = 0
        register.addTarget(self, action: #selector(push), for: .touchUpInside)
        
    }
    @objc func push(){
        let vc = self.storyboard!.instantiateViewController(withIdentifier:"FourthViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
